<?php /* #?ini charset="utf-8"?

[ImageMagick]
IsEnabled=true
ExecutablePath=/usr/bin
Executable=convert
Filters[]=strippaexif=-strip
Filters[]=customquality=-quality %1

Filters[]=thumb=-resize 'x%1' -resize '%1x<' -resize 50%
Filters[]=centerimg=-gravity center -crop %1x%2+0+0 +repage
Filters[]=strip=-strip
Filters[]=sharpen=-sharpen 0.5
Filters[]=geometry/scalemin=-geometry %1x%2^
Filters[]=forcecenter=-background %1 -gravity center -extent %2x%3

#centra l'immagine e la ritaglia
Filters[]=roundcenterimg=-gravity center -crop %1x%2+0+0 +repage
#applica la maschera
Filters[]=roundmask=extension/openpa_theme_2014/design/standard/images/round-mask.png -composite
#scontorna il bianco
Filters[]=roundtrans=-transparent White

[MIMETypeSettings]
# Set JPEG quality from 0 (worst quality, smallest file) to 100 (best quality, biggest file)
Quality[]=image/jpeg;70

[AliasSettings]
AliasList[]
AliasList[]=reference
AliasList[]=small
AliasList[]=medium
AliasList[]=large
AliasList[]=rss
AliasList[]=logo
AliasList[]=icon

#altre estensioni
AliasList[]=opengraph
AliasList[]=carousel
AliasList[]=squaremini
AliasList[]=squarethumb
AliasList[]=squaremedium
AliasList[]=imagefull
AliasList[]=imagefullwide
AliasList[]=imagefull_cutwide
AliasList[]=widethumb
AliasList[]=widemini
AliasList[]=widemedium

[opengraph]
Reference=reference
Filters[]
Filters[]=geometry/scale=400;400

[carousel]
Reference=reference
Filters[]
Filters[]=geometry/scalewidth=1200
Filters[]=centerimg=1200;800

[squaremini]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=100
Filters[]=centerimg=50;50

[squarethumb]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=300
Filters[]=centerimg=150;150

[squaremedium]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=250
Filters[]=centerimg=250;250

[imagefull]
Reference=reference
Filters[]=geometry/scalewidthdownonly=850

[imagefullwide]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=1170

[imagefull_cutwide]
Reference=reference
Filters[]=geometry/scalewidthdownonly=850
Filters[]=centerimg=850;380

[widemini]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=150
Filters[]=centerimg=100;50

[widemedium]
Reference=reference
Filters[]
Filters[]=geometry/scaleheight=250
Filters[]=geometry/forcecenter=#ffffff;400;250

[widethumb]
Reference=reference
Filters[]
Filters[]=geometry/scalewidthdownonly=300
Filters[]=geometry/forcecenter=#ffffff;300;180

[original]
Filters[]

[reference]
Filters[]
Filters[]=geometry/scaledownonly=1170;1170
Filters[]=strippaexif
Filters[]=customquality=75

[small]
Reference=reference
Filters[]
Filters[]=geometry/scaledownonly=100;160

[medium]
Reference=reference
Filters[]
Filters[]=geometry/scaledownonly=200;290

[large]
Reference=reference
Filters[]
Filters[]=geometry/scaledownonly=360;440

[rss]
Reference=reference
Filters[]
Filters[]=geometry/scale=88;31

[logo]
Reference=reference
Filters[]
Filters[]=geometry/scaleheight=36

[marker]
Reference=
Filters[]
Filters[]=geometry/scale=30;30
*/ ?>

<?php /* #?ini charset="utf-8"?

[IndexOptions]
OptimizeOnCommit=disabled

[SolrFieldMapSettings]
CustomMap[ezobjectrelation]=ocSolrDocumentFieldObjectRelation
CustomMap[ezobjectrelationlist]=ocSolrDocumentFieldObjectRelation
CustomMap[ezdate]=ocSolrDocumentFieldDate
CustomMap[ezdatetime]=ocSolrDocumentFieldDate
CustomMap[ezinteger]=ocSolrDocumentFieldInteger

DatatypeMap[ezobjectrelationlist]=string
DatatypeMap[ezinteger]=sint

DatatypeMapSort[ezstring]=string
DatatypeMapSort[ezinteger]=sint

DatatypeMapFilter[ezstring]=string
DatatypeMapFilter[ezinteger]=sint

DatatypeMapFacet[ezinteger]=sint

[IndexExclude]
ClassIdentifierList[]=file

[HighLighting]
Enabled=false

*/ ?>

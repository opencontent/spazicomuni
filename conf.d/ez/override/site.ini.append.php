<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=
Password=
Database=
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[FileSettings]
VarDir=

[SiteSettings]
SiteName=
SiteURL=
DefaultAccess=ita_booking
SiteList[]
SiteList[]=ita_booking
SiteList[]=backend

[HTTPHeaderSettings]
CustomHeader=disabled
OnlyForAnonymous=enabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200

[VarnishSettings]
VarnishHostName=
VarnishPort=
VarnishServers[]

[SearchSettings]
LogSearchStats=disabled

[RoleSettings]
PolicyOmitList[]=user/do_logout
PolicyOmitList[]=exportas/csv
PolicyOmitList[]=exportas/custom
PolicyOmitList[]=ezinfo/is_alive
PolicyOmitList[]=opendata/api

[Session]
SessionNameHandler=custom
CookieSecure=true
CookieHttponly=true
Handler=ezpSessionHandlerPHP
ForceStart=disabled

[SiteAccessSettings]
RemoveSiteAccessIfDefaultAccess=enabled
ForceVirtualHost=true
DebugAccess=enabled
CheckValidity=false
MatchOrder=uri
HostMatchMapItems[]
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ita_booking
RelatedSiteAccessList[]=backend
AvailableSiteAccessList[]
AvailableSiteAccessList[]=ita_booking
AvailableSiteAccessList[]=backend

[MailSettings]
TransportAlias[smtp]=OpenPASMTPTransport
Transport=smtp
TransportConnectionType=tls
TransportServer=
TransportPort=
TransportUser=
TransportPassword=
AdminEmail=
EmailSender=
SenderHost=
BlackListEmailDomains[]
BlackListEmailDomainSuffixes[]

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
LogoutRedirect=/?logout
RegistrationEmail=

[TimeZoneSettings]
TimeZone=Europe/Rome

[RegionalSettings]
TextTranslation=enabled

[DebugSettings]
DebugToolbar=disabled

[UserFormToken]
CookieHttponly=true
CookieSecure=0

*/ ?>

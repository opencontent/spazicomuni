<?php /* #?ini charset="utf-8"?

[ClusteringSettings]
FileHandler=eZDFSFileHandler

[eZDFSClusteringSettings]
MountPointPath=/mnt/efs/cluster-openpa/
DBBackend=eZDFSFileHandlerPostgresqlBackend
DBName=
DBHost=
DBPort=
DBSocket=
DBUser=
DBPassword=
DBConnectRetries=3
DBExecuteRetries=20
MaxCopyRetries=5


*/ ?>

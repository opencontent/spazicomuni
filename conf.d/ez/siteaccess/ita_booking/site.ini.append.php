<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ezmbpaex
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=openpa_booking
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocsocialuser
ActiveAccessExtensions[]=ocsocialdesign
ActiveAccessExtensions[]=openpa_theme_2014
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=occodicefiscale
ActiveAccessExtensions[]=openpa_userprofile
ActiveAccessExtensions[]=ocwebhookserver
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ocopendata_forms

[SiteSettings]
LoginPage=embedded
IndexPage=openpa_booking/home
DefaultPage=openpa_booking/home

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=booking
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=social
AdditionalSiteDesignList[]=ocbootstrap
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=ita-IT
ContentObjectLocale=ita-IT
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=ita-IT
TextTranslation=enabled
TranslationSA[]
LanguageSA[]

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
Rules[]=module;content/tipafriend
Rules[]=module;settings/edit

*/ ?>
